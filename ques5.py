
def main():
    inp = input()
    if inp == "hello":
        print("hi")
    elif inp == "hey":
        print("you", len(inp))
    elif len(inp) > 5:
        result = mystery(inp)
        print(result)
        
def mystery(inp):
    y = 0
    newstr = ""
    for item in inp:
        if item == " ":
            y += 1
        else:
            newstr += item
    y += 1
    print(y)

    low = len(newstr)
    return low

main()

# Question:
# What will be the output of the following code if user enters
# a) “hello"
# b) “hi"
# c) “CS 104 is fun"
# d) “bye"
# Write no output if you think that no output is produced.
#
# Answer:
# a) hi
# b) No output
# c) 
# 4
# 10
# d) No output
